// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"js/gameOfThrones.js":[function(require,module,exports) {
var gameApp = window.gameApp || {};

gameApp = function () {
  var $el = $('.game');
  var $startBtn = $el.find('.game-btn-start');
  var $reloadBtn = $el.find('.game-btn-reload');
  var $introLayer = $el.find('.game-layer-intro');
  var $cardsLayer = $el.find('.game-layer-cards');
  var $resultLayer = $el.find('.game-layer-result');
  var $steps = $el.find('.game-data-steps');
  var $cards = $el.find('.game-cards');
  var $result = $resultLayer.find('.game-result-desc');
  var $resultSteps = $resultLayer.find('.game-result-steps');
  var $loader = $el.find('.loader');
  var $share = $el.find('.share > div');
  var baseUrl;
  var cards = [{
    photo: 'https://b1.m24.ru/c/1183807.580xp.jpg',
    photoAuthor: '',
    title: 'Кристиан Бейл',
    desc: 'Номинирован на «Оскар» за яркий и убедительный портрет Дика Чейни — самого влиятельного вице-президента в истории США (фильм «Власть»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183793.580xp.jpg',
    photoAuthor: '',
    title: 'Гленн Клоуз',
    desc: 'Номинирована на «Оскар» за роль Джоан — отчаянной домохозяйки с писательским амбициями, слишком любящей своего мужа (фильм «Жена»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183796.580xp.jpg',
    photoAuthor: '',
    title: 'Адам Драйвер',
    desc: 'Номинирован на «Оскар» за роль Флипа — поразительно уравновешенного детектива и члена Ку-клукс-клана под прикрытием (фильм «Чёрный клановец»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183795.580xp.jpg',
    photoAuthor: '',
    title: 'Леди Гага',
    desc: 'Номинирована на «Оскар» за роль Элли — амбициозной и страстной молодой артистки (фильм «Звезда родилась»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183794.580xp.jpg',
    photoAuthor: '',
    title: 'Уиллем Дефо',
    desc: 'Номинирован на «Оскар» за роль Винсента ван Гога — сумасшедшего гения, мечтающего когда-нибудь стать знаменитым художником (фильм «Ван Гог. На пороге вечности»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183792.580xp.jpg',
    photoAuthor: '',
    title: 'Реджина Кинг',
    desc: 'Номинирована на «Оскар» за роль Шэрон — матери, мечтающей о счастье для дочери, которая стала заложницей обстоятельств (в фильме «Если Бил-стрит могла бы заговорить»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183791.580xp.jpg',
    photoAuthor: '',
    title: 'Сэм Рокуэлл',
    desc: 'Номинирован на «Оскар» за почти безупречный портрет Джорджа Буша-младшего, 43-го президента США (фильм  «Власть»)'
  }, {
    photo: 'https://b1.m24.ru/c/1183797.580xp.jpg',
    photoAuthor: '',
    title: 'Эми Адамс',
    desc: 'Номинирована на «Оскар» за роль Линн Чейни — супруги экс-вице-президента США (фильм  «Власть»)'
  }]; // $share.attr('data-url', location.href)
  // $share.attr('data-image', 'https://b1.m24.ru/c/1184080.jpg')

  var cardsQnt = cards.length * 2;
  var step = 0;

  var createNewPLayer = function createNewPLayer() {
    initCards();
  };

  function declOfNum(n, titles) {
    return titles[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
  }

  var url = '../images/oscars3.svg';

  var initCards = function initCards() {
    var randomCards = cards.slice(0);
    var cardsHTML = '';

    function shuffle(a) {
      var j, x, i;

      for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
      }
    }

    for (var i = 0, l = cards.length; i < l; i++) {
      randomCards.push(randomCards[i]);
    }

    shuffle(randomCards);

    for (var i = 0, l = randomCards.length; i < l; i++) {
      cardsHTML += '<div class="game-cards-item">' + '<div class="game-card" data-photo="' + randomCards[i]['photo'] + '" data-photo-author="' + randomCards[i]['photoAuthor'] + '" data-title="' + randomCards[i]['title'] + '" data-desc="' + randomCards[i]['desc'] + '">' + '<div class="game-card-side game-card-cover"></div>' + '<div class="game-card-side game-card-photo" style="background-image: url(' + randomCards[i]['photo'] + ')"></div>' + '</div>' + '</div>';

      if (i == randomCards.length - 1) {
        $cards.html(cardsHTML);
        $el.addClass('is-start');
      }
    }
  };

  var selectCard = function selectCard(card) {
    var previous = $cards.find('.active');

    if (!card.hasClass('active') && !card.hasClass('done') && previous.length < 2) {
      card.addClass('active');

      if (previous.length) {
        var photo = card.data('photo');
        var photoAuthor = card.data('photo-author');
        var title = card.data('title');
        var desc = card.data('desc');

        if (previous.data('title') === title) {
          previous.addClass('done');
          card.addClass('done');
          setTimeout(function () {
            showDesc(photo, photoAuthor, title, desc);
          }, 500);
        }

        setTimeout(function () {
          $steps.text(++step);
          previous.removeClass('active');
          card.removeClass('active');
        }, 1000);
      }
    }
  };

  var showDesc = function showDesc(photo, photoAuthor, title, desc) {
    var descPopup = $('<div class="game-cards-desc">' + '<div class="game-cards-desc-photo" style="background-image:url(' + photo + ')"></div>' + '<div class="game-cards-desc-photo-author">' + photoAuthor + '</div>' + '<h3>' + title + '</h3>' + '<p>' + desc + '</p>' + '<div class="game-cards-desc-btn"><button class="game-btn">Продолжить</button></div>' + '</div>');
    $el.addClass('popup-visible').append(descPopup);
  };

  var showResult = function showResult() {
    $el.addClass('is-complete');
    $resultSteps.html('<b>' + step + '</b>' + declOfNum(step, [' ход', ' хода', ' ходов']));
  };

  var init = function init(options) {
    baseUrl = options.baseUrl;
    $el.show();
    $startBtn.on('click', function (e) {
      $steps.text(0);
      createNewPLayer(); // $share.attr('data-image', '')
    });
    $cards.on('click', '.game-card', function (e) {
      var self = $(this);
      selectCard(self);
    });
    $('body').on('click', '.game-cards-desc button', function () {
      var self = $(this);
      var completedCardsQnt = $cards.find('.done').length;
      self.parents('.game-cards-desc').remove();
      $el.removeClass('popup-visible');

      if (completedCardsQnt === cardsQnt) {
        showResult();
      }
    });
    $reloadBtn.on('click', function (e) {
      $el.removeClass('is-complete');
      step = 0;
      $steps.text(step);
      $cards.empty(); // $share.attr('data-image', 'http://media.interactive.netuse.gr/filesystem/images/20190116/low/aeg-still-0369_1581_107712670.JPG')

      initCards();
    });
  };

  return {
    init: init
  };
}();

$(function () {
  gameApp.init({
    baseUrl: ''
  });
});
},{}],"C:/Users/Алексей/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "31254" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["C:/Users/Алексей/AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","js/gameOfThrones.js"], null)
//# sourceMappingURL=/gameOfThrones.6ebe5e27.js.map